﻿namespace ur5.models
{
	//установление информации о сборке проекта(?)
    public class URVersion
    {
        public string ProjectName { get; set; }

        public int Major { get; set; }
        public int Minor { get; set; }
        public int Revision { get; set; }
        public string Build { get; set; }
        public URVersion(string project_name, int major_version, int minor_version, int svn_revision, string build_date)
        {
            this.ProjectName = project_name;
            this.Major = major_version;
            this.Minor = minor_version;
            this.Revision = svn_revision;
            this.Build = build_date;
        }


        public override string ToString()
        {
            return $"<{ProjectName}> {Major}.{Minor}.{Revision}.{Build}";
        }

        public static URVersion CurrentVersion = new URVersion("<unamed>", 0, 0, 0, "<unknown>");
    }
}