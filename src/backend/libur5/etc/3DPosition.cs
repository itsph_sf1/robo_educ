﻿namespace ur5.etc
{
	//класс для определение позиции робота по координатам
    public class Position3D
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
    }
}