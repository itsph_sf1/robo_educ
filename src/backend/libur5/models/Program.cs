﻿namespace ur5.models
{
    using System;
    using System.Threading;
    using core;
    using urscript.interactive;

	//главный класс ()
    public class Program
    {
        public DateTime StartTime { get; set; } = default;
        public DateTime EndTime { get; set; } = default;
        public string Name { get; set; }
        public string UID { get; set; }
        public SourceFile Source { get; set; }
        public ProgramStatus Status { get; set; } = ProgramStatus.NonStarted;

		
        public Program(string name, string uid, SourceFile source = null)
        {
            this.Name = name;
            this.Source = source;
            this.UID = uid;
        }

        public ProgramStatus Start()
        {
            StartTime = DateTime.UtcNow;
            EndTime = default;
            return Status = ProgramStatus.Playing;
        }

        public ProgramStatus Stop()
        {
            EndTime = DateTime.UtcNow;
            return Status = ProgramStatus.Ended;
        }

        public ProgramStatus Pause() => Status = ProgramStatus.Paused;

        public ProgramStatus Resume() => Status = ProgramStatus.Playing;
    }

	//перечисление
    public enum ProgramStatus
    {
        NonStarted,
        Playing,
        FailedToPlay,
        Paused,
        Ended,
        UnknownError
    }
}