﻿namespace ur5.models
{
	//установление начальных значений
    public class ModbusSignal
    {
        public string Name { get; set; } = "<unnamed>";
        public int Value { get; set; } = -1;
        public int ResponseTimeInMs = -1;
        public int TimeSinceLastUpdateInMs { get; set; } = -1;
        public int ConnectionStatus { get; set; } = -1;
    }
}