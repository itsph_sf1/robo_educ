﻿namespace urscript.interactive
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class PositionSyntax : IConstructorSyntax
    {
        public static string defaultSpeed { get; set; } = "0.800";//скорость манипулятора по умолчанию
        public static string defaultAcceleration { get; set; } = "0.900";//ускорение манипулятора по умолчанию
        public IEnumerable<string> Position { get; set; }//текцщая позиция манипулятора
        public string Speed { get; set; }//текущая скорость манипулятора
        public string Acceleration { get; set; }//текущее ускорение манипулятора
        public IResult Metadata { get; set; }//метадата
        public string TargetInstruction { get; set; } = "Joint";//следующие координаты перемещения манипулятора

        public string Construct()
        {
            var speed = Speed ?? defaultSpeed;
            var acc = Acceleration ?? defaultAcceleration;
            var pos = Position.ToList();

            switch (TargetInstruction)
            {
                case "Joint": TargetInstruction = "movej"; break;
                case "Linear": TargetInstruction = "movel"; break;
                case "Circular": TargetInstruction = "movec"; break;
                default: throw new RuntimeException($"UR0105: Unknown moving type: {TargetInstruction}");
            }

            if (pos.Count != 6)
                throw new Exception($"UR0103: Compilation Error: 6 coordinates expected at {Metadata.Remainder.Line}:0");

            return $"{TargetInstruction}([{pos[0]},{pos[1]},{pos[2]},{pos[3]},{pos[4]},{pos[5]}],{acc},{speed},0,0)";
        }
    }
}