﻿namespace ur5.models
{
    using System.Collections.Generic;
    using System.Linq;
    using JetBrains.Annotations;

	//
    public class ModbusInfoMessage
    {
        private readonly Stack<ModbusSignal> signalList = new Stack<ModbusSignal>();
        [PublicAPI]
		//добавление сигнала
        public void AddSignal(string name, int value, int responseTime, int signalTimeSinceLastUpdateInMs,
            int signalConnectionStatus)
        {
            var sig = new ModbusSignal
            {
                ConnectionStatus = signalConnectionStatus,
                TimeSinceLastUpdateInMs = signalTimeSinceLastUpdateInMs,
                ResponseTimeInMs = responseTime,
                Value = value
            };

            if (name != null)
                sig.Name = name;

            signalList.Push(sig);
        }
        [PublicAPI]
        public List<ModbusSignal> getSignals() => signalList.ToArray().ToList();

        public override string ToString()
        {
            var box = signalList.Select(x =>
                    $"[{x.Name}, {x.Value}, {x.ConnectionStatus}, {x.ResponseTimeInMs}ms, {x.TimeSinceLastUpdateInMs}ms]")
                .ToArray();
            return string.Join(", ", box);
        }
    }
}