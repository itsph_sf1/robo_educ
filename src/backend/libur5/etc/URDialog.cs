﻿namespace ur5.etc
{
    using System;

	//класс с выводами сообщений в консоль
    public static class URDialog
    {
        public static void Notifity(string key, string message)
        {
            Console.WriteLine($"URDialog [{key}]: {message}");
        }
		
		//безопасность остановки
        public static void SecurityStoppedDialog()
        {
            Console.WriteLine($"URDialog [SecurityStopped]");
        }

        public static void BadRobotStateDialog(string toString)
        {
            Console.WriteLine($"URDialog [BadState]: {toString}");

        }
    }
}