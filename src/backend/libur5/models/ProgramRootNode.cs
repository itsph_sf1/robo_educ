﻿namespace ur5.models
{
	
    public class ProgramRootNode : ProgramNode
    {
        public string Name { get; set; }
        public string FileName { get; set; }
        public bool HasChanged { get; set; }
        public bool HasSaved { get; set; }
        public int Version { get; set; }
        public string SerialNumberRobot { get; set; }

        public override bool isUndefined()
        {
            throw new System.NotImplementedException();
        }



		//переход в директорию db
        public static string getDirectoryDB() => "./db/";
    }
}