﻿namespace ur5.models
{
    using System;
    using System.Collections.Generic;
    using etc;
	
	//программный узел
    public abstract class ProgramNode
    {
        protected int ID { get; set; } = -1;
        protected List<ProgramNode> PastExecutionNodes = new List<ProgramNode>();
        protected List<ProgramNode> NextExecutionNodes = new List<ProgramNode>();
        protected ProgramNode RootNode { get; set; }


        public void programChange()
        {
            try
            {
                RootNode?.programChange();
            }
            catch (Exception e)
            {
                LibLog.Warn($":::programChange without program root:::. [{e.Message}]", this);
            }

        }



        public abstract bool isUndefined();
        public void setSelected()
        {
           // ((ProgramRootNode)RootNode).setSelected(this);
        }
    }
}