﻿namespace urscript.interactive
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    /// <summary>
    /// Source code file and module management class
    /// </summary>
    public static class SourceManager
    {
        /// <summary>
        /// List of modules\source files
        /// </summary>
        public static List<SourceFile> DB = new List<SourceFile>();
        /// <summary>
        /// Root folder where all modules and source files are located
        /// </summary>
        private static string RootPath { get; set; }//точка прибытия
        /// <summary>
        /// Indexing modules in root path
        /// </summary>
        public static void IndexingSourceFiles(string dir)
        {
            if(DB.Count != 0)
                return;
            RootPath = dir;
            var files = Directory.GetFiles(dir, "*.frag", SearchOption.AllDirectories).Select(x => new SourceFile(x)).ToArray();
            if (!files.Any())
                throw new RuntimeException("SourceManager: Zero files detected.");
            DB.AddRange(files);
        }
        /// <summary>
        /// Reindexing modules in root 
        /// </summary>
        private static void ReIndexing()
        {
            var files = Directory.
                GetFiles(RootPath, "*.frag", SearchOption.AllDirectories).
                Select(x => new SourceFile(x)).
                ToArray();
            foreach (var file in files)
            {
                if(DB.FirstOrDefault(x => x.Name == file.Name) != null)
                    continue;
                DB.Add(file);
            }
        }
        /// <summary>
        /// Invoking module
        /// </summary>
        /// <param name="name">
        /// name of module
        /// </param>
        /// <param name="reindex">
        /// Try to re-index modules in case of error?
        /// </param>
        public static SourceFile InvokeModule(string name, bool reindex = true)
        {
            var result = DB.FirstOrDefault(x => string.Equals(x.Name, name, StringComparison.CurrentCultureIgnoreCase));
            if (result != null) return result;

            if (!reindex)
                throw new Exception($"Module '{name}' not found.");
            ReIndexing();
            return InvokeModule(name, false);
        }

        public static SourceFile SafeInvokeModule(string name)
        {
            try
            {
                return InvokeModule(name);
            }
            catch
            {
                return null;
            }
        }
    }
}